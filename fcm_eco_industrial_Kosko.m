function fcm_eco_industrial_Kosko(A)

%A=input('enter the vector of initial concept values-18 concepts')

% Fisrt scenario. 

W=[0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0
0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0
0	0.5	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0
0	0	0	0	0	0	-0.5	0	0	0	0	0	0	0	0	0	0	0
-1	-1	0	0	0	0.5	0	0	0	0	0	0	0	0	0	0	0	0
0.5	0	1	0	0	0	0	0	1	0	0	0	0	-1	0	0	0	0.5
0	0	0	-0.5	0	0	0	0	0	0	0	0	0	0	0	0	0	0
1	0	0	0	0	0	0	0	0	0	0	0.5	0	0	0	0	0	0
0	0	0	0	0	0	0	0	0	0	0	0	1	0	0	0	0	0
0	0	0	0	0	-0.5	0	0	0	0	-1	0	0	0	0	0	-0.5	0
0	0	0	0	0	0	0	0	0	0	0	0	0	-0.5	0	0	0	0
0	0	0	0	0	0.5	0	0	0	0	0	0	0	-0.5	0	0	0	0
0	0	0	1	0	0	0	0.5	0	0	0.5	0	0	0	0.5	0.5	0.5	0
0	0	0	0	0	0	0	0	0	-0.5	0	0	0	0	0	0	0	0
0	0	0	0	0	0	0	0	0	0	0.5	0	0	0	0	0	0	0
0.5	0	0.5	0	0	0	0	0	1	0	0	0	0	-1	0	0	0	0.5
0.5	0	0.5	0	0	0	0	0	1	0	0	0	0	-1	0	0	0	0.5
-1	-1	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0
];

X=[]; % final vector after one calculation step (following a path- no any feedback or iteration)

Ao=A;
X=[X;A];
   
 for r=1:30
     X=[X;A];
      A=((A)*W)%+(A);
    %A=(A*W)+A;
   for j=1:18
       % if A(1,j)==Ao(1,j);  
         %       A(1,j)=Ao(1,j); 
          %  else
                A(1,j)=1/(1+exp(-(A(1,j))));
       % end
   end
 end
 
   if r > 2
      C = abs(X(r-1,:)-X(r,:)) < 0.0001*ones(1,18);  
      if all(C)
         disp('The number of repetition is:');
         disp(r);
         disp('Values of nodes');
         disp(X);
         plot(X);
         xlabel('Number of repetition');
         ylabel('Value of node');
         disp('Press any keybutton to continue');
         pause;
         main1
      end
   end


 X=A;

    disp('The final concepts values are:');
    disp(X);
    
 clf;